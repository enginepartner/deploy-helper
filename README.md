# Deploy Helper

[![Build Status](https://semaphoreci.com/api/v1/engine-partner/deploy-helper/branches/master/badge.svg)](https://semaphoreci.com/engine-partner/deploy-helper)

This project builds a Docker image that contains the scripts for automating
deployment easier.

The key advantage is that projects of the same kind can use the scripts in this
image instead of having the copies of the same scripts.
